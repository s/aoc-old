package aoc.y2017

import aoc.Day

object Day04 extends Day {

  implicit class ArrayOps[A](a: Array[A]) {

    def unique: Boolean = a.distinct.length == a.length

  }

  override def part1(input: String): String =
    input.lines.map(_.split(' ')).count(_.unique).toString

  override def part2(input: String): String =
    input.lines.map(_.split(' ').map(_.sorted)).count(_.unique).toString

}
