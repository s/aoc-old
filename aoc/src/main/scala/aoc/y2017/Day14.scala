package aoc.y2017

import aoc.Day

object Day14 extends Day {

  def getRows(input: String): Seq[BigInt] = {
    val keyStrings = (0 to 127).map(row => s"$input-$row")
    val knotHashes = keyStrings.map(Day10.knotHash)
    knotHashes.map(BigInt(_, 16))
  }

  override def part1(input: String): String = {
    val bigInts = getRows(input)
    val bits = bigInts.map(_.bitCount).sum
    bits.toString
  }

  override def part2(input: String): String = {
    val bigInts = getRows(input)
    val disk = bigInts.toVector.map { bigInt =>
      (0 to bigInt.bitLength).toVector.map(bigInt.testBit).padTo(128, false).take(128)
    }
    type GroupedDisk = Vector[Vector[Option[Int]]]
    val numberBlocks: GroupedDisk = disk.zipWithIndex.map {
      case (row, y) => row.zipWithIndex.map {
        case (true, x) => Some(y * 128 + x)
        case (false, _) => None
      }
    }
    def simplifyGroups(disk: GroupedDisk): GroupedDisk = {
      disk.zipWithIndex.map {
        case (row, y) => row.zipWithIndex.map {
          case (None, _) => None
          case (Some(g), x) =>
            val left = if(x > 0) disk(y)(x - 1) else None
            val right = if(x < 127) disk(y)(x + 1) else None
            val up = if(y > 0) disk(y - 1)(x) else None
            val down = if(y < 127) disk(y + 1)(x) else None
            Vector(Some(g), left, right, up, down).collect { case Some(v) => v }.minOption
        }
      }
    }
    lazy val defrags: LazyList[GroupedDisk] = numberBlocks #:: defrags.map(simplifyGroups)
    val (simplest, _) = defrags.zipWithIndex.takeWhile {
      case (thisPass, index) => index < 1 || defrags(index - 1) != thisPass
    }.last
    simplest.flatten.collect { case Some(g) => g }.groupBy(identity).size.toString
  }

}
