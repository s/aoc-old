package aoc.y2017

import aoc.Day

object Day09 extends Day {

  import fastparse._, MultiLineWhitespace._

  sealed trait Elem {
    def score(level: Int = 1): Int
    def garbageCount: Int
  }
  case class Group(contents: List[Elem]) extends Elem {
    override def score(level: Int = 1): Int = level + contents.map(_.score(level + 1)).sum
    override def garbageCount: Int = contents.map(_.garbageCount).sum
  }
  case class Garbage(contents: String) extends Elem {
    override def score(level: Int = 1): Int = 0
    override def garbageCount: Int = {
      val r = """!.""".r
      contents.length - (r.findAllIn(contents).length * 2)
    }
  }

  def elemParser[_: P]: P[Elem] = P(groupParser | garbageParser)
  def groupParser[_: P]: P[Group] = P("{" ~/ elemParser.rep(0, ","./) ~ "}").map(c => Group(c.toList))
  def garbageCharacterParser[_: P]: P[Unit] = P(("!".? ~ CharIn("""aeiou"'<{,}""")) | "!>" | "!!")./
  def garbageParser[_: P]: P[Garbage] = P("<" ~/ garbageCharacterParser.rep.! ~ ">").map(s => Garbage(s))

  def getElem(input: String): Elem = {
    val Parsed.Success(e, _) = parse(input, elemParser(_))
    e
  }

  override def part1(input: String): String = {
    val e = getElem(input)
    e.score().toString
  }

  override def part2(input: String): String = {
    val e = getElem(input)
    e.garbageCount.toString
  }

}
