package aoc.y2017

import aoc.Day

object Day01 extends Day {

  override def part1(input: String): String = {
    val reg = """(\d)(?=\1)""".r
    val matches = reg.findAllIn(input).toList
    val csum = matches.map(_.toInt).sum
    if (input.head == input.last) {
      (csum + input.head.toString.toInt).toString
    } else {
      csum.toString
    }
  }

  override def part2(input: String): String = {
    val (left, right) = input.splitAt(input.length / 2)
    val zipped = left.zip(right)
    val doubled = zipped.filter { case (l, r) => l == r }.map { case (c, _) => c.toString }
    (doubled.map(_.toInt).sum * 2).toString
  }

}
