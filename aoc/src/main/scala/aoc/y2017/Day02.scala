package aoc.y2017

import aoc.Day

object Day02 extends Day {

  override def part1(input: String): String = {
    val lines = input.lines.toList
    val rows = lines.map(_.split("""\s+""").map(_.toInt))
    val minMax = rows.map(r => (r.min, r.max))
    val differences = minMax.map { case (min, max) => max - min }
    differences.sum.toString
  }

  override def part2(input: String): String = {
    val lines = input.lines.toList
    val rows = lines.map(_.split("""\s+""").map(_.toInt))
    rows.flatMap { r =>
      val q = r.zipWithIndex.flatMap {
        case (e, i) =>
          r.drop(i + 1)
            .map(f => {
              val (min, max) = (Math.min(e, f), Math.max(e, f))
              if (max % min == 0) {
                Some(max / min)
              } else {
                None
              }
            })
      }
      q.filter(_.isDefined).map(_.get)
    }.sum.toString
  }

}
