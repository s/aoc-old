package aoc.y2017

import aoc.Day

import scalax.collection.Graph
import scalax.collection.GraphPredef._, scalax.collection.GraphEdge._
import scalax.collection.edge.Implicits._

object Day12 extends Day {

  import fastparse._, SingleLineWhitespace._

  def channelNameParser[_: P]: P[Int] = P(CharIn("0-9").rep(1).!).map(_.toInt)
  def connectionListParser[_: P]: P[Seq[Int]] = P(channelNameParser.rep(1, ","./))

  def channelParser[_: P]: P[Seq[DiEdge[Int]]] = P(channelNameParser ~ "<->" ~ connectionListParser).map {
    case (name, connections) => connections.map(name ~> _)
  }

  def channelsParser[_: P]: P[Graph[Int, DiEdge]] =
    P(channelParser.rep(1, "\n"./)).map(s => {
      val allEdges = s.flatten
      Graph.apply(allEdges: _*)
    })

  def getGraph(input: String): Graph[Int, DiEdge] = {
    val Parsed.Success(graph, _) = parse(input, channelsParser(_))
    graph
  }

  def findGroup(graph: Graph[Int, DiEdge])(startingNode: graph.NodeT): Set[graph.NodeT] =
    graph.nodes.filter(_.pathTo(startingNode).isDefined).toSet

  override def part1(input: String): String = {
    val graph = getGraph(input)
    val zero = graph get 0
    graph.nodes.count(_.pathTo(zero).isDefined).toString
  }

  override def part2(input: String): String = {
    val graph = getGraph(input)
    def groups(g: Graph[Int, DiEdge], count: Int = 0): Int = {
      if (g.isEmpty) {
        count
      } else {
        val node = g.nodes.toList.head
        val group = findGroup(g)(node)
        groups(g --! group, count + 1)
      }
    }
    groups(graph).toString
  }

}
