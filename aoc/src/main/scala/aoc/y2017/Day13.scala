package aoc.y2017

import aoc.Day

object Day13 extends Day {

  import fastparse._, SingleLineWhitespace._

  def parseNumber[_: P]: P[Int] = P(CharIn("0-9").rep(1).!).map(_.toInt)
  def parseScanner[_: P]: P[(Int, Int)] = P(parseNumber ~ ": " ~ parseNumber)
  def parseScanners[_: P]: P[Map[Int, Int]] = P(parseScanner.rep(1, "\n")).map(Map(_: _*))

  def scannerAtStart(time: Int, range: Int): Boolean = (time % (2 * (range - 1))) == 0

  def scanners(input: String): Map[Int, Int] = parse(input, parseScanners(_)).get.value
  def severity(scanners: Map[Int, Int], delay: Int): Int = scanners.collect {
    case (index, range) if scannerAtStart(index + delay, range) => index * range
  }.sum

  override def part1(input: String): String = {
    severity(scanners(input), delay = 0).toString
  }

  override def part2(input: String): String = {
    val parsedScanners = scanners(input)
    lazy val packetDelays: LazyList[Int] = LazyList.from(0)
    lazy val tripSeverities = packetDelays.map(delay => (delay, severity(parsedScanners, delay)))
    val avoidanceDelay = tripSeverities.indexWhere {
      case (delay, severity) => severity == 0 && delay % (2 * (parsedScanners(0) - 1)) != 0
    }
    avoidanceDelay.toString
  }

}
