package aoc.y2017

import aoc._

object Day10 extends Day {

  def knot(ring: List[Int], by: Int, at: Int): List[Int] = {
    val shifted = rotateLeft(ring, at)
    val (sHead, sTail) = shifted.splitAt(by)
    val spliced = sHead.reverse ++ sTail
    rotateRight(spliced, at)
  }

  def runShifts(shifts: List[Int]): List[Int] = {
    val ring = (0 to 255).toList
    val (last, _) = shifts.zipWithIndex.foldLeft((ring, 0)) {
      case ((cr, acc), (shift, jump)) =>
        val k = knot(cr, shift, acc)
        val i = acc + shift + jump
        (k, i)
    }
    last
  }

  def knotHash(input: String): String = {
    val magic = List(17, 31, 73, 47, 23)
    val codes = input.map(_.toInt) ++ magic
    val shifts = List.fill(64)(codes).flatten
    val last = runShifts(shifts)
    val hashGroups = last.grouped(16)
    val xored = hashGroups.map(_.reduce(_ ^ _))
    val hexed = xored.map(_.toHexString.reverse.padTo(2, '0').reverse.toString)
    hexed.mkString
  }

  override def part1(input: String): String = {
    val shifts = input.split(',').map(_.toInt).toList
    val last = runShifts(shifts)
    (last.head * last.tail.head).toString
  }

  override def part2(input: String): String = {
    knotHash(input)
  }

}
