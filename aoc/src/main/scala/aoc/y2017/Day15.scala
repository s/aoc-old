package aoc.y2017

import aoc.Day

import scala.annotation.tailrec

object Day15 extends Day {

  import fastparse._, NoWhitespace._

  def parseNumber[_: P]: P[Int] = P(CharIn("0-9").rep(1).!).map(_.toInt)
  def parseGenerator[_: P](name: String): P[Int] = P("Generator " ~ name ~ " starts with " ~ parseNumber)
  def parseGenerators[_: P]: P[(Int, Int)] = P(parseGenerator("A") ~ "\n" ~ parseGenerator("B"))

  @tailrec def step(number: Int, multiplier: Int, forceMultiple: Int = 1): Int = {
    val product = number.toLong * multiplier.toLong
    val remainder = (product % Int.MaxValue).toInt
    if(remainder % forceMultiple != 0) step(remainder, multiplier, forceMultiple)
    else remainder
  }

  @tailrec def go(genANum: Int, genBNum: Int, genAMult: Int, genBMult: Int, forceMultipleA: Int = 1, forceMultipleB: Int = 1, left: Int = 40_000_000, sum: Int = 0): Int = {
    if(left < 0) sum
    else {
      val nextA = step(genANum, genAMult, forceMultipleA)
      val nextB = step(genBNum, genBMult, forceMultipleB)
      val nextLeft = left - 1
      val nextSum = if(((nextA ^ nextB) & ((1 << 16) - 1)) == 0) sum + 1 else sum
      go(nextA, nextB, genAMult, genBMult, forceMultipleA, forceMultipleB, nextLeft, nextSum)
    }
  }

  def run(input: String, pairs: Int, forceMultiple: Boolean): Int = {
    val genAMult = 16807
    val genBMult = 48271
    val (genAStart, genBStart) = parse(input, parseGenerators(_)).get.value
    go(genAStart, genBStart, genAMult, genBMult, if(forceMultiple) 4 else 1, if(forceMultiple) 8 else 1, pairs)
  }

  override def part1(input: String): String = {
    run(input, pairs = 40_000_000, forceMultiple = false).toString
  }

  override def part2(input: String): String = {
    run(input, pairs = 5_000_000, forceMultiple = true).toString
  }

}
