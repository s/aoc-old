package aoc.y2019

import aoc.Day

object Day04 extends Day {

  import fastparse._, SingleLineWhitespace._

  def parseNumber[_: P]: P[Int] = P(CharIn("0-9").rep(1).!).map(_.toInt)
  def parseRange[_: P]: P[Range.Inclusive] = P(parseNumber ~ "-" ~ parseNumber).map {
    case (a, b) => a to b
  }

  def validPassword(password: Int, range: Range, repetitionBounded: Boolean): Boolean = {
    lazy val isSixDigits = password.toString.length == 6
    lazy val isWithinRange = range.contains(password)
    lazy val hasRepetition =
      if(!repetitionBounded)
        password.toString.toSeq.groupBy(identity).exists(_._2.length >= 2)
      else
        password.toString.toSeq.groupBy(identity).exists(_._2.length == 2)
    lazy val isIncreasing = password.toString.toSeq.sorted == password.toString.toSeq

    isSixDigits && isWithinRange && hasRepetition && isIncreasing
  }

  def numberValidPasswords(rangeString: String, repetitionBounded: Boolean): Int = {
    val range = parse(rangeString, parseRange(_)).get.value
    range.count(validPassword(_, range, repetitionBounded))
  }

  override def part1(input: String): String = {
    numberValidPasswords(input, repetitionBounded = false).toString
  }

  override def part2(input: String): String = {
    numberValidPasswords(input, repetitionBounded = true).toString
  }

}
