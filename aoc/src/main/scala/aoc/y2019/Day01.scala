package aoc.y2019

import aoc.Day

object Day01 extends Day {

  def fuel(mass: Int): Int = (mass / 3) - 2

  override def part1(input: String): String = {
    val masses = input.linesIterator.map(_.toInt)
    masses.map(fuel).sum[Int].toString
  }

  override def part2(input: String): String = {
    val masses = input.linesIterator.map(_.toInt)
    masses.map { initial =>
      lazy val fuels: LazyList[Int] = fuel(initial) #:: fuels.map(fuel)
      fuels.takeWhile(_ > 0).sum
    }.sum[Int].toString
  }

}
