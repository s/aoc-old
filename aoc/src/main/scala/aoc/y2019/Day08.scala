package aoc.y2019

import aoc.Day

object Day08 extends Day {

  val imgWidth = 25
  val imgHeight = 6

  override def part1(input: String): String = {
    val image = input.map(_ - '0').toVector.grouped(imgWidth).toVector.grouped(imgHeight).toVector
    val layerWithLeastZeros = image.minBy(_.map(_.count(_ == 0)).sum)
    (layerWithLeastZeros.map(_.count(_ == 1)).sum * layerWithLeastZeros.map(_.count(_ == 2)).sum).toString
  }

  override def part2(input: String): String = {
    val image = input.map(_ - '0').toVector.grouped(imgWidth).toVector.grouped(imgHeight).toVector
    val message = image.foldLeft(Vector.fill(imgHeight, imgWidth)(2)) { (image, vector) =>
      image.zip(vector).map {
        case (imageRow, vectorRow) =>
          imageRow.zip(vectorRow).map {
            case (2, v) => v
            case (o, _) => o
          }
      }
    }
    "\n" ++ message.map(_.map {
      case 0 => " "
      case 1 => "#"
    }.mkString).mkString("\n")
  }

}
