package aoc

package object y2019 extends Year {

  override def days: Map[String, Day] = Map(
    "1" -> Day01,
    "2" -> Day02,
    "3" -> Day03,
    "4" -> Day04,
    "5" -> Day05,
    "6" -> Day06,
    "7" -> Day07,
    "8" -> Day08,
    "9" -> Day09,
  )

}
