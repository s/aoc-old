package aoc.y2019.intcode

import cats._
import cats.implicits._
import cats.data.State

case class Machine(
  memory: Map[BigInt, BigInt] = Map.empty,
  pc: BigInt = 0,
  input: Vector[BigInt] = Vector.empty,
  output: Vector[BigInt] = Vector.empty,
  relativeBase: BigInt = 0
)

object Machine {
  def apply(code: Vector[BigInt]): Machine =
    Machine(code.zipWithIndex.map[(BigInt, BigInt)] { case (v, i) => (i, v) }.toMap)

  def parseInstruction(instructions: Map[Int, Instruction] = Instruction.defaultInstructions): Operation[(Instruction, Flags)] =
    for {
      loc <- location
      instructionAndFlags <- get(loc)
      instruction = instructions((instructionAndFlags % 100).toInt)
      flags = {
        val flagString = instructionAndFlags.toString.reverseIterator.drop(2)
        val flagIterator = flagString.map(_ - '0')
        LazyList.from(flagIterator) ++ LazyList.continually(0)
      }
    } yield (instruction, flags)
  def step(instructions: Map[Int, Instruction] = Instruction.defaultInstructions, operandModes: OperandModes = OperandMode.defaultOperandModes): Step =
    for {
      (instruction: Instruction, flags: Flags) <- parseInstruction(instructions)
      _ <- advance(1)
      result <- instruction.run(operandModes, flags)
    } yield result
  def run(instructions: Map[Int, Instruction] = Instruction.defaultInstructions, operandModes: OperandModes = OperandMode.defaultOperandModes, haltCondition: Operation[Boolean] = isHalted): Operation[Vector[Unit]] =
    step(instructions, operandModes).untilM(haltCondition)

  val noop: Step = State.pure(())

  def goto(address: BigInt): Step = State.modify(_.copy(pc = address))
  val location: Operation[BigInt] = State.inspect(_.pc)
  def advance(by: BigInt): Step = for { loc <- location; go <- goto(by + loc) } yield go

  val getOffset: Operation[BigInt] = State.inspect(_.relativeBase)
  def setOffset(offset: BigInt): Step = State.modify(_.copy(relativeBase = offset))
  def adjustOffset(adjustment: BigInt): Step = for { offset <- getOffset; set <- setOffset(offset + adjustment) } yield set

  def set(address: BigInt, value: BigInt): Step = State.modify(m => m.copy(m.memory.updated(address, value)))
  def get(address: BigInt): Operation[BigInt] = State.inspect(_.memory.getOrElse(address, 0))

  def input(value: BigInt): Step = State.modify(m => m.copy(input = m.input :+ value))
  val pull: Operation[BigInt] = State(m => (m.copy(input = m.input.tail), m.input.head))

  def output(value: BigInt): Step = State.modify(m => m.copy(output = m.output :+ value))

  val isHalted: Operation[Boolean] = for {
    loc <- location
    instruction <- get(loc)
  } yield instruction % 100 == 99
  val waitingForInput: Operation[Boolean] = for {
    loc <- location
    instruction <- get(loc)
    onInputInstruction = instruction % 100 == 3
    noInput <- State.inspect[Machine, Boolean](_.input.isEmpty)
  } yield onInputInstruction && noInput

}
