package aoc.y2019

import cats.data.{Kleisli, State}

package object intcode {

  type Flags = LazyList[Int]

  type Operation[A] = State[Machine, A]
  type Step = Operation[Unit]

  type Instruction = Kleisli[Operation, (OperandModes, Flags), Unit]

  type OperandModes = Map[Int, OperandMode]

}
