package aoc.y2019

import aoc.Day
import aoc.y2019.intcode.{Machine, Step}
import cats._
import cats.implicits._
import cats.data.State

object Day02 extends Day {

  def initialReplacement(noun: Int, verb: Int): Step = Machine.set(1, noun) *> Machine.set(2, verb)

  def runMachine(memory: Vector[BigInt], noun: Int, verb: Int): BigInt = {
    val initialState = Machine(memory)
    val runMachine = for {
      _ <- initialReplacement(noun, verb)
      ran <- Machine.run()
    } yield ran
    val endState = runMachine.runS(initialState).value
    endState.memory(0)
  }

  override def part1(input: String): String = {
    val memory = input.split(",").map(BigInt(_)).toVector
    runMachine(memory, 12, 2).toString
  }

  override def part2(input: String): String = {
    val memory = input.split(",").map(BigInt(_)).toVector
    val magic = 19690720
    val solutions = for {
      noun <- 0 to 99
      verb <- 0 to 99 if runMachine(memory, noun, verb) == magic
    } yield noun * 100 + verb
    solutions.head.toString
  }

}
