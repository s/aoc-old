package aoc.y2019

import cats._
import cats.implicits._
import aoc.Day
import aoc.y2019.intcode.{Machine, Step}
import cats.data.State

object Day05 extends Day {

  def diagnostic(input: String, diagnosticNumber: Int): BigInt = {
    val memory = input.split(",").map(BigInt(_)).toVector
    val initialState = Machine(memory)
    val machineInput = diagnosticNumber
    val runMachine: Step = for {
      _ <- Machine.input(machineInput)
      ran <- Machine.run()
    } yield ran.last
    val endState = runMachine.runS(initialState).value
    endState.output.last
  }

  override def part1(input: String): String = {
    diagnostic(input, 1).toString
  }

  override def part2(input: String): String = {
    diagnostic(input, 5).toString
  }

}
