package aoc.y2019

import aoc.Day
import aoc.y2019.intcode.Machine

object Day09 extends Day {

  def boost(input: String, value: BigInt): BigInt = {
    val code = input.split(",").map(BigInt(_)).toVector
    val initialState = Machine(code)
    val runMachine = for {
      _ <- Machine.input(value)
      ran <- Machine.run()
    } yield ran
    runMachine.runS(initialState).value.output.last
  }

  override def part1(input: String): String = boost(input, 1).toString

  override def part2(input: String): String = boost(input, 2).toString
}
