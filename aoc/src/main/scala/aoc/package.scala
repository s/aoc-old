package object aoc {

  def rotateLeft[A](list: List[A], i: Int): List[A] = {
    val size = list.size
    list.drop(i % size) ++ list.take(i % size)
  }

  def rotateRight[A](list: List[A], i: Int): List[A] = {
    val size = list.size
    list.drop(size - (i % size)) ++ list.take(size - (i % size))
  }

  implicit class TupleInt2Ops(t: (Int, Int)) {

    def +(o: (Int, Int)): (Int, Int) = (t, o) match {
      case ((x1, y1), (x2, y2)) => (x1 + x2, y1 + y2)
    }

    def *(m: Int): (Int, Int) = t match {
      case (a, b) => (a * m, b * m)
    }

  }

  def uniquePrefix[A](as: Stream[A]): Stream[A] = {
    def go(as: Stream[A], seen: Set[A]): Stream[A] = as match {
      case h #:: t if !seen(h) => h #:: go(t, seen + h)
      case _ => Stream.empty[A]
    }
    go(as, Set.empty)
  }

}
