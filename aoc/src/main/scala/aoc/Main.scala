package aoc

import scala.io.StdIn

object Main {

  val years = Map(
    "2016" -> y2016.!,
    "2017" -> y2017.!,
    "2018" -> y2018.!,
    "2019" -> y2019.!,
  )

  def main(args: Array[String]): Unit = {
    val year = askForYear()
    val day = askForDay(year)
    val part = askForPart(day)
    println("Problem input? End input with an `End of Input` line.")
    val input = LazyList.continually(StdIn.readLine()).takeWhile(_ != "End of Input").toList.mkString("\n")
    val startTime = System.currentTimeMillis()
    val output = part(input)
    val endTime = System.currentTimeMillis()
    println(s"Output: $output")
    println(s"Time: ${endTime - startTime}ms")
  }

  def askForYear(): Year = {
    println(s"Years: ${years.keys.toList.sortBy(_.toInt).mkString(", ")}")
    print("Year? ")
    val input = StdIn.readLine()
    years.get(input) match {
      case Some(y) => y
      case None =>
        println("Not a valid year.")
        askForYear()
    }
  }

  def askForDay(y: Year): Day = {
    println(s"Days: ${y.days.keys.toList.sortBy(_.toInt).mkString(", ")}")
    print("Day? ")
    val input = StdIn.readLine()
    y.days.get(input) match {
      case Some(d) => d
      case None =>
        println("Not a valid day.")
        askForDay(y)
    }
  }

  def askForPart(d: Day): String => String = {
    print("Part (1/2)? ")
    val input = StdIn.readLine()
    input match {
      case "1" => d.part1
      case "2" => d.part2
      case _ =>
        println("Not a valid part.")
        askForPart(d)
    }
  }

}
