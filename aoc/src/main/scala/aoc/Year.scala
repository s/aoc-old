package aoc

trait Year {

  def !(): this.type = this

  def days: Map[String, Day]

}
