package aoc.y2018

import aoc._

object Day02 extends Day {

  override def part1(input: String): String = {
    val lines = input.lines.toList
    val groups = lines.map(_.groupBy(identity))
    val twos = groups.count(_.values.exists(_.length == 2))
    val threes = groups.count(_.values.exists(_.length == 3))
    (twos * threes).toString
  }

  override def part2(input: String): String = {
    val lines = input.lines.toList
    val distanceMap = lines.flatMap(s => lines.map(c => ((s, c), s.intersect(c)))).toMap
    val (_, r) = distanceMap.filter { case ((a, _), d) => d.length != a.length }.maxBy { case (_, d) => d.length }
    r.mkString
  }

}
