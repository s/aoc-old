package aoc.y2018
import aoc.Day

object Day05 extends Day {

  def pair(c1: Char, c2: Char): Boolean = {
    val c1l = c1.isLower
    val c2l = c2.isLower
    (c1l && !c2l && c1 == c2.toLower) || (!c1l && c2l && c1.toLower == c2)
  }

  def simpleLength(s: String): Int = {
    s.foldLeft("") {
        case (stack, ne) => {
          if (stack.isEmpty) {
            ne +: stack
          } else {
            val peek = stack.head
            if (pair(ne, peek)) {
              stack.tail
            } else {
              ne +: stack
            }
          }
        }
      }
      .length
  }

  override def part1(input: String): String = {
    simpleLength(input).toString
  }

  override def part2(input: String): String = {
    val allChars = input.map(_.toLower).toSeq.distinct.sortBy(c => input.count(_ == c))
    val mapped = allChars.map(c => simpleLength(input.filter(_.toLower != c)))
    mapped.min.toString
  }

}
