package aoc.y2018

import aoc._

object Day01 extends Day {

  override def part1(input: String): String = {
    input.lines.map(_.toInt).sum.toString
  }

  override def part2(input: String): String = {
    println(input.lines.size)
    val i: List[Int] = input.lines.map(_.toInt).toList
    val inet = i.zipWithIndex.map {
      case (f, index) => f + i.take(index).sum
    }
    val net = i.sum
    val fcs = 0 #:: Stream.iterate(inet)(cl => cl.map(_ + net)).flatten
    val us = uniquePrefix(fcs)
    val nf = fcs(us.size)
    nf.toString
  }

}
