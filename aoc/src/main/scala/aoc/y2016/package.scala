package aoc

package object y2016 extends Year {

  override def days: Map[String, Day] = Map(
    "1" -> Day01
  )

}
