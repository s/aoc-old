package aoc

trait Day {

  def part1(input: String): String
  def part2(input: String): String

}
