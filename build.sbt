import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

val sharedSettings = Seq(
  name         := "aoc",
  organization := "tf.bug",
  version      := "0.1.0",
  scalaVersion := "2.13.1",
  libraryDependencies ++= List(
    "com.lihaoyi" %%% "fastparse"      % "2.1.3",
    "org.scala-graph" %%% "graph-core" % "1.13.1",
    "org.typelevel" %%% "cats-core"    % "2.0.0",
    "org.typelevel" %%% "cats-free"    % "2.0.0",
  ),
  mainClass := Some("tf.bug.aoc.Main"),
  addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full),
  addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1"),
  scalacOptions ++= Seq(
    "-Ywarn-value-discard"
  )
)

// lazy val aoc = crossProject(/* JSPlatform, */ JVMPlatform /* , NativePlatform */ )
//   .crossType(CrossType.Pure)
//   .settings(sharedSettings)
//
// lazy val aocJS = aoc.js
// lazy val aocJVM = aoc.jvm
// lazy val aocNative = aoc.native

lazy val aoc = (project in file("aoc")).settings(sharedSettings)
